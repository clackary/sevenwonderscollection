//
//  DetailViewController.h
//  SevenWondersCollection
//
//  Created by Zachary Cole on 2/29/16.
//  Copyright © 2016 Zachary Cole. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WonderOfTheWorldObject.h"

@interface DetailViewController : UIViewController
@property (nonatomic, weak) WonderOfTheWorldObject* info;
@property (strong, nonatomic) UILabel *titleLbl;
@property (strong, nonatomic) UILabel *locationLbl;
@property (strong, nonatomic) UIImageView *imageView;
@end
