//
//  DetailViewController.m
//  SevenWondersCollection
//
//  Created by Zachary Cole on 2/29/16.
//  Copyright © 2016 Zachary Cole. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 100, 400, 350)];
    self.titleLbl = [[UILabel alloc] initWithFrame: CGRectMake(20, 500, 400, 40)];
    self.titleLbl.backgroundColor = [UIColor greenColor];
    self.locationLbl = [[UILabel alloc] initWithFrame: CGRectMake(20, 560, 400, 40)];
    self.locationLbl.backgroundColor = [UIColor greenColor];
    [self.view addSubview:self.imageView];
    [self.view addSubview:self.titleLbl];
    [self.view addSubview:self.locationLbl];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    self.titleLbl.text = self.info.name;
        NSLog(@"%@", self.info.name);
    self.locationLbl.text = self.info.location;
    self.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.info.image]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
