//
//  ViewController.m
//  SevenWondersCollection
//
//  Created by Zachary Cole on 2/23/16.
//  Copyright © 2016 Zachary Cole. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

typedef void(^myCompletion)(NSArray* data);

- (void)viewDidLoad {
    [super viewDidLoad];


    

    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    cView=[[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:layout];
    [cView setDataSource:self];
    [cView setDelegate:self];
    
    [cView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [cView setBackgroundColor:[UIColor blueColor]];
    
    [self.view addSubview:cView];
    
    
    arrayOfSevenWonders = [NSMutableArray new];
    
    [self getSevenWondersInfoWithResponseBlock:^(NSArray *array) {
        
        for (NSDictionary* d in array) {
            WonderOfTheWorldObject* wotwo = [WonderOfTheWorldObject new];
            wotwo.image = d[@"image"];
            wotwo.image_thumb = d[@"image_thumb"];
            wotwo.location = d[@"location"];
            wotwo.name = d[@"name"];
            wotwo.region = d[@"region"];
            wotwo.wikipedia = d[@"wikipedia"];
            wotwo.year_built = d[@"year_built"];
            
            [arrayOfSevenWonders addObject:wotwo];
            
//            NSURL *url = [NSURL URLWithString:wotwo.image_thumb];
//            
//            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//            
//            NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
//            
//            [[session dataTaskWithRequest:request
//                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [cView reloadData];
//                                //Your main thread code goes in here
//                                self.imageview.image = [UIImage imageWithData:data];
                            });
//
//                            
//                            
//                        }] resume];
        }
        
    }];
}


-(void)getSevenWondersInfoWithResponseBlock:(myCompletion) completion {
    NSString *urlString = [NSString stringWithFormat: @"http://aasquaredapps.com/Class/sevenwonders.json"];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [[session dataTaskWithRequest:request
                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                    
                    // do stuff
                    completion([NSJSONSerialization JSONObjectWithData:data options:0 error:&error]);
                    
                }] resume];
}

#pragma collectionView stuff
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrayOfSevenWonders.count;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    UIImageView* imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200, 200)];
    WonderOfTheWorldObject *w = [arrayOfSevenWonders objectAtIndex:indexPath.row];

    UIImage* img = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:w.image_thumb]]]; //seems like this is slowing things down.
    imgView.image = img;
    [cell addSubview:imgView];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.bounds.size.width/2 - 5, 250);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"here");

    
    DetailViewController* dvc = [DetailViewController new];
    dvc.info = [arrayOfSevenWonders objectAtIndex:indexPath.row];
//    [self.navigationController pushViewController:dvc animated:YES];
    [self.navigationController pushViewController:dvc animated:YES];
//    [nvc release];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end