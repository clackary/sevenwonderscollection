//
//  main.m
//  SevenWondersCollection
//
//  Created by Zachary Cole on 2/23/16.
//  Copyright © 2016 Zachary Cole. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
