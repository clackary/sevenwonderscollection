//
//  FirstViewController.m
//  SevenWondersCollection
//
//  Created by Zachary Cole on 3/1/16.
//  Copyright © 2016 Zachary Cole. All rights reserved.
//

#import "FirstViewController.h"

@interface FirstViewController ()

@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    ViewController *viewController = [[ViewController alloc] init];
    nvc = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    [self addChildViewController:nvc];
    [self.view addSubview:nvc.view];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
