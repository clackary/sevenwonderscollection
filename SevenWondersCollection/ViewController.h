//
//  ViewController.h
//  SevenWondersCollection
//
//  Created by Zachary Cole on 2/23/16.
//  Copyright © 2016 Zachary Cole. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WonderOfTheWorldObject.h"
#import "DetailViewController.h"

@interface ViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
{
    NSMutableArray* arrayOfSevenWonders;

    UICollectionView* cView;
}
@property (weak, nonatomic) IBOutlet UIImageView *imageview;

@end

